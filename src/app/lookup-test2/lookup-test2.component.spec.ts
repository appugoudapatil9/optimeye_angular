import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookupTest2Component } from './lookup-test2.component';

describe('LookupTest2Component', () => {
  let component: LookupTest2Component;
  let fixture: ComponentFixture<LookupTest2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookupTest2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupTest2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
