
import { Router } from '@angular/router';
import { LookupModel } from './../../model/lookupmodel';
import { LookupService } from './../../service/lookup.service';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-dashboard',
  templateUrl: './searchlookup.component.html',
  styleUrls: ['./searchlookup.component.scss']
})
export class SearchlookupComponent implements OnInit {

    lookups: LookupModel[];
  constructor(private router: Router, private lookupService: LookupService) { }

  ngOnInit() {
this.lookupService.getAllLookups().
subscribe(data => {
this.lookups = data;
});
  }

  editLookup(lookupId: string): void {
    localStorage.setItem('lookupId', lookupId);
    this.router.navigate(['editlookup']);
  }
}
