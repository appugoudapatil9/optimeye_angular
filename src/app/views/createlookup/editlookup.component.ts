import { LookupModel } from './../../model/lookupmodel';
import { LookupService } from './../../service/lookup.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './editlookup.component.html',
    styleUrls: ['./editlookup.component.scss']
})

export class EditlookupComponent implements OnInit {

    angForm: FormGroup;
    lookupmodel: LookupModel = new LookupModel();
    constructor(private route: ActivatedRoute,
        private router: Router,
        private lookupService: LookupService,
        private fb: FormBuilder) { }


        createForm() {
            this.angForm = this.fb.group({
                lookupType: ['', Validators.required ],
                lookupName: ['', Validators.required ],
                lookupDescription: ['', Validators.required ],
                activeFlag: ['', Validators.required ],
                startDate: ['', Validators.required ],
                endDate: ['', Validators.required ]
                   });
              }

    ngOnInit() {
        this.route.params.subscribe(params => {

            this.lookupService.editLookup(params['lookupId']).subscribe(res => {

                this.lookupmodel = <LookupModel>res;
            });
        });
    }
}
