import { EditlookupComponent } from './editlookup.component';
import { SearchlookupComponent } from './searchlookup.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatelookupComponent } from './createlookup.component';

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Look up'
      },
      children: [
        {
          path: 'createlookup',
          component: CreatelookupComponent,
          data: {
            title: 'Add Lookup'
          }
        },
        {
          path: 'searchlookup',
          component: SearchlookupComponent,
          data: {
            title: 'Search Lookup'
          }
        },
        {
          path: 'editlookup',
          component: EditlookupComponent,
          data: {
            title: 'Edit Lookup'
          }
        }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class LookupRoutingModule { }
