import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatelookupComponent } from './createlookup.component';

describe('CreatelookupComponent', () => {
  let component: CreatelookupComponent;
  let fixture: ComponentFixture<CreatelookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatelookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatelookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
