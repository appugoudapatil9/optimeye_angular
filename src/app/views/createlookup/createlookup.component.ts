
import { LookupService } from './../../service/lookup.service';
import { Component, OnInit } from '@angular/core';
import { LookupModel } from '../../model/lookupmodel';

@Component({
  selector: 'app-dashboard',
  templateUrl: './createlookup.component.html',
  styleUrls: ['./createlookup.component.scss']
})
export class CreatelookupComponent implements OnInit {

  lookupmodel: LookupModel = new LookupModel();
  submitted = false;
  successMsg;
  errorMsg;
  constructor(private lookupService: LookupService) { }

  ngOnInit() {
  }

  save() {
    this.lookupService.createLookup(this.lookupmodel)
      .subscribe(data => {
        console.log(data);
        this.successMsg = data.responseMessage;
      },
      err => {
        // console.error(err);
        this.errorMsg = <any>err;
        console.log(this.errorMsg);
      },
      () => {
      this.lookupmodel = new LookupModel();
      });
    // this.customer = new Customer();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
