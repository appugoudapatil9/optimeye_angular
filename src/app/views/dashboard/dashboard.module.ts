import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RuleService } from './../../optimize/rule/rule.service';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
 import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as Widgets from 'fusioncharts/fusioncharts.widgets';

import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

FusionChartsModule.fcRoot(FusionCharts, Widgets, FusionTheme);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DashboardRoutingModule,
    ChartsModule,
    BsDropdownModule,
    FusionChartsModule,
    ButtonsModule.forRoot()
  ],
  declarations: [ DashboardComponent],
  providers: [RuleService],
})
export class DashboardModule { }
