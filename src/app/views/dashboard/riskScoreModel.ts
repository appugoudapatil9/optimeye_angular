export class RiskScoreModel {

    rulesource: string;
    riskContributingScore: number;
    enterpriseRiskScore: number;
    complainceRiskScore: number;
    internalAuditRiskScore: number;
    systemRiskScore: number;
    riskType: string;
    riskScore: number;
}
