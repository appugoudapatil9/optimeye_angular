import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { AuthGuard } from './optimize/shared/guards/auth.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'lookup',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/lookup/lookup.module#LookupModule'
      },
      {
        path: 'project',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/optimize.module#OptimizeModule'
      },
      {
        path: 'stakeholder',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/optimize.module#OptimizeModule'
      },
      {
        path: 'riskcategory',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/riskcategory/riskcategory.module#RiskcategoryModule'
      },
      {
        path: 'program',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/optimize.module#OptimizeModule'
      },
      {
        path: 'company',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/company/company.module#CompanyModule'
      },
      {
        path: 'user',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/users/user.module#UserModule'
      },
      {
        path: 'rule',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/rule/rule.module#RuleModule'
      },
      {
        path: 'rule',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './optimize/rule/rule.module#RuleModule'
      }
      // ,
      // {
      //   path: 'icons',
      //   loadChildren: './views/icons/icons.module#IconsModule'
      // },
      // {
      //   path: 'notifications',
      //   loadChildren: './views/notifications/notifications.module#NotificationsModule'
      // },
      // {
      //   path: 'theme',
      //   loadChildren: './views/theme/theme.module#ThemeModule'
      // },
      // {
      //   path: 'widgets',
      //   loadChildren: './views/widgets/widgets.module#WidgetsModule'
      // }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
