import { RiskCategoryModel } from './riskcategorymodel';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RiskCategoryService } from './riskcategory.service';
import { LookupModel } from '../lookup/lookupmodel';


@Component({
  selector: 'app-dashboard',
  templateUrl: './createriskcategory.component.html',
  styleUrls: ['./createriskcategory.component.scss'],
})
export class CreateriskcategoryComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  riskcategoryModel: RiskCategoryModel = new RiskCategoryModel();
  riskcategoryModels: RiskCategoryModel[] = [];
  submitted = false;
  successMsg;
  errorMsg;
  riskCategoryData: LookupModel[];
  riskSubCategoryData: LookupModel[];


  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  constructor(private riskCategoryService: RiskCategoryService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.addrows();
     this.fetchRiskCategories();
     this.fetchRiskSubCategories();
    }

    addrows() {
      for (let i = 0; i < 10; i++) {
        const newItem = new RiskCategoryModel();
        this.riskcategoryModels.push(newItem);
      }
    }

  fetchRiskCategories() {
      this.riskCategoryService.getAllRiskCategory()
      .subscribe(
        riskCategories => {
          this.riskCategoryData = <LookupModel[]>riskCategories;
        },
        );
  }

  fetchRiskSubCategories() {
    this.riskCategoryService.getAllRiskSubCategory()
      .subscribe(
        riskSubCategories => {
          this.riskSubCategoryData = <LookupModel[]>riskSubCategories;
        },
        );
  }

  save() {
        this.riskCategoryService.createRiskCategory(this.riskcategoryModels)
      .subscribe(data => {
        this.successMsg = data.responseMessage;
      },
      err => {
        this.errorMsg = <any>err;
      },
      () => {
      this.riskcategoryModel = new RiskCategoryModel();
      });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

onDateSelectionDone(type): void {
    if (type === 'to') {
        this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
        this.isFromDatepickerVisible = false;
    }
}

toggleCalendar(type: string) {
    if (type === 'from') {
        this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
        this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
        this.isToDatepickerVisible = !this.isToDatepickerVisible;
        this.isFromDatepickerVisible = false;
    }
}
}
