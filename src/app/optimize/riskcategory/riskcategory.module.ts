import { EditriskCategoryComponent } from './editriskcategory.component';
import { SearchriskcateoryComponent } from './searchriskcategory.component';
import { RiskCategoryService } from './riskcategory.service';

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RiskCategoryRoutingModule } from './riskcategory-routing.module';
import { CreateriskcategoryComponent } from './createriskcategory.component';

@NgModule({
    imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      NgbModule,
      FormsModule,
      DatepickerModule,
      BsDatepickerModule,
      RiskCategoryRoutingModule

    ],
    declarations: [
      CreateriskcategoryComponent,
      SearchriskcateoryComponent,
      EditriskCategoryComponent,
    ],
    providers: [RiskCategoryService]
  })

export class RiskcategoryModule { }
