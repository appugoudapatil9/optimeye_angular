
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RiskCategoryService {

  constructor(private http: HttpClient) { }

  // private baseUrl = 'http://localhost:8080/risk';
  private baseUrl = 'http://3.17.60.48:8080/risk';

  createRiskCategory(riskCategoryModel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/createRiskCategory`, riskCategoryModel);
  }

  upadateRiskCategory(riskCategoryModel: Object): Observable<any> {
    return this.http.put(`${this.baseUrl}` + `/updateRiskCategory`, riskCategoryModel);
  }

  searchRiskCategory(riskCategoryModel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/searchRiskCategory`, riskCategoryModel);
  }

  getAllRiskCategory(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/riskCategory`);
  }

  getAllRiskSubCategory(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/riskSubCategory`);
  }

  editRiskCategory(id) {
    return this
          .http
          .get(`${this.baseUrl}/${id}`);
    }
}
