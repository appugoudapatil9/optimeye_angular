import { RiskCategoryService } from './riskcategory.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { LookupModel } from '../lookup/lookupmodel';
import { RiskCategoryModel } from './riskcategorymodel';


@Component({
  selector: 'app-dashboard',
  templateUrl: './searchriskcategory.component.html',
  styleUrls: ['./searchriskcategory.component.scss'],
})
export class SearchriskcateoryComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  riskcateorymodel: RiskCategoryModel = new RiskCategoryModel();
  submitted = false;
  successMsg;
  errorMsg;
  riskCategoryData: LookupModel[];
  riskSubCategoryData: LookupModel[];
  riskData: RiskCategoryModel[];
  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  constructor(private riskCategoryService: RiskCategoryService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.fetchRiskCategories();
    this.fetchRiskSubCategories();
    }

    fetchRiskCategories() {
        this.riskCategoryService.getAllRiskCategory()
        .subscribe(
          riskCategories => {
            this.riskCategoryData = <LookupModel[]>riskCategories;
          },
          );
    }

    fetchRiskSubCategories() {
      this.riskCategoryService.getAllRiskSubCategory()
        .subscribe(
          riskSubCategories => {
            this.riskSubCategoryData = <LookupModel[]>riskSubCategories;
          },
          );
    }

  search() {
    this.riskCategoryService.searchRiskCategory(this.riskcateorymodel)
      .subscribe(data => {
        this.riskData = data;
      },
      err => {
      },
      () => {
      this.riskcateorymodel = new RiskCategoryModel();
      });
  }

  onSubmit() {
    this.submitted = true;
    this.search();
  }

onDateSelectionDone(type): void {
    if (type === 'to') {
        this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
        this.isFromDatepickerVisible = false;
    }
}

toggleCalendar(type: string) {
    if (type === 'from') {
        this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
        this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
        this.isToDatepickerVisible = !this.isToDatepickerVisible;
        this.isFromDatepickerVisible = false;
    }
}

}
