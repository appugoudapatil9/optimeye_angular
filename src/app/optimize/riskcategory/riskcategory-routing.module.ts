import { EditriskCategoryComponent } from './editriskcategory.component';
import { SearchriskcateoryComponent } from './searchriskcategory.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateriskcategoryComponent } from './createriskcategory.component';

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Riskcateory'
      },
      children: [
        {
          path: 'createriskcateory',
          component: CreateriskcategoryComponent,
          data: {
            title: 'Create Risk Category'
          }
         },
        {
          path: 'searchriskcateory',
          component: SearchriskcateoryComponent,
          data: {
            title: 'Search Risk Category'
          }
        },
        {
            path: 'editRiskCategory',
            component: EditriskCategoryComponent,
            data: {
              title: 'Edit Risk Category'
            }
        }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class RiskCategoryRoutingModule { }
