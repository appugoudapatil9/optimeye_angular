import { RiskCategoryService } from './riskcategory.service';
import { LookupModel } from './../../model/lookupmodel';
import { RiskCategoryModel } from './riskcategorymodel';

import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

import * as moment from 'moment';
import { formatDate } from '@angular/common';

@Component({
    selector: 'app-dashboard',
    templateUrl: './editriskcategory.component.html',
    styleUrls: ['./editriskcategory.component.scss']
})

export class EditriskCategoryComponent implements OnInit {

  riskcateorymodel: RiskCategoryModel = new RiskCategoryModel();
  submitted = false;
  successMsg;
  errorMsg;
  riskCategoryData: LookupModel[];
  riskSubCategoryData: LookupModel[];

    constructor(private route: ActivatedRoute,
        private router: Router,
        private riskCategoryService: RiskCategoryService) { }

    ngOnInit() {
        this.fetchRiskCategories();
        this.fetchRiskSubCategories();
        this.route.queryParams.subscribe(params => {
            console.log(params);
            this.riskCategoryService.editRiskCategory(params['idRiskcategory']).subscribe(res => {
                this.riskcateorymodel = <RiskCategoryModel>res;
            });
        });
    }
    fetchRiskCategories() {
        this.riskCategoryService.getAllRiskCategory()
        .subscribe(
          riskCategories => {
            this.riskCategoryData = <LookupModel[]>riskCategories;
          },
          );
    }

    fetchRiskSubCategories() {
      this.riskCategoryService.getAllRiskSubCategory()
        .subscribe(
          riskSubCategories => {
            this.riskSubCategoryData = <LookupModel[]>riskSubCategories;
          },
          );
    }

  save() {
    this.riskCategoryService.upadateRiskCategory(this.riskcateorymodel)
  .subscribe(data => {
    this.successMsg = data.responseMessage;
  },
  err => {
    this.errorMsg = <any>err;
  },
  () => {
  this.riskcateorymodel = new RiskCategoryModel();
  });

}

onSubmit() {
this.submitted = true;
this.save();
}
}
