import { EditriskCategoryComponent } from './editriskcategory.component';
import { SearchriskcateoryComponent } from './searchriskcategory.component';
import { CreateriskcategoryComponent } from './createriskcategory.component';

import { Route } from '@angular/router';

export const RiskcateoryRoutes: Route[] = [
    {
        path: 'createriskcateory',
        component: CreateriskcategoryComponent
    } ,
    {
        path: 'searchriskcateory',
        component: SearchriskcateoryComponent
    },
     {
        path: 'editRiskCategory',
        component: EditriskCategoryComponent
    }
];

