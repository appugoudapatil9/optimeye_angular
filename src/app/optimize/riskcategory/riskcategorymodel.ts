export class RiskCategoryModel {

    idRiskcategory: number;
    idLookupRiskCategory: number;
    riskCategoryName: string;
    idLookupRiskSubCategory: number;
    riskSubCategoryName: string;
    impact: number;
    frequency: string;
}
