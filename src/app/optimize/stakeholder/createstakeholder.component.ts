import { StakeholderModel } from './stakeholdermodel';
import { ProjectModel } from './../project/projectmodel';
import { EmployeeModel } from './../project/employeemodel';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StakeholderService } from './stakeholder.service';
import { LookupModel } from '../lookup/lookupmodel';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-dashboard',
  templateUrl: './createstakeholder.component.html',
  styleUrls: ['./createstakeholder.component.scss'],
})
export class CreatestakeholderComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  stakeholdermodel: StakeholderModel = new StakeholderModel();
  stakeholderModels: StakeholderModel[] = [];
  submitted = false;
  successMsg;
  errorMsg;
  employeeModels: EmployeeModel[];
  typeData: LookupModel[];
  classificationData: LookupModel[];
  projectModels: ProjectModel[];
  closeResult: string;
  index = -1;

  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  constructor(private stakeholderService: StakeholderService, private route: ActivatedRoute,
    private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.addrows();
    this.fetchEmployees();
    this.fetchTypes();
    this.fetchClassifications();
    this.fetchProjects();
  }

  addrows() {
    for (let i = 0; i < 10; i++) {
      const newItem = new StakeholderModel();
      this.stakeholderModels.push(newItem);
    }
  }

  open(content, index, stakeholdermodel: StakeholderModel) {

    this.stakeholdermodel = stakeholdermodel;
    this.index = index;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  addData(modalStakeholderData: StakeholderModel) {
    this.stakeholdermodel = modalStakeholderData;
    this.stakeholderModels[this.index] = this.stakeholdermodel;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  fetchEmployees() {
    this.stakeholderService.getAllEmployees()
      .subscribe(
        employeeData => {
          this.employeeModels = <EmployeeModel[]>employeeData;
        },
      );
  }

  fetchTypes() {
    this.stakeholderService.getAllTypes()
      .subscribe(
        types => {
          this.typeData = <LookupModel[]>types;
        },
      );
  }

  fetchClassifications() {
    this.stakeholderService.getAllClassifications()
      .subscribe(
        classifications => {
          this.classificationData = <LookupModel[]>classifications;
        },
      );
  }

  fetchProjects() {
    this.stakeholderService.getAllProjects()
      .subscribe(
        projectsData => {
          this.projectModels = <ProjectModel[]>projectsData;
        },
      );
  }


  save() {
    this.stakeholderService.createStakeholders(this.stakeholderModels)
      .subscribe(data => {
        this.successMsg = data.responseMessage;
      },
        err => {
          this.errorMsg = <any>err;
        },
        () => {
          this.stakeholdermodel = new StakeholderModel();
        });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  onDateSelectionDone(type): void {
    if (type === 'to') {
      this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
      this.isFromDatepickerVisible = false;
    }
  }

  toggleCalendar(type: string) {
    if (type === 'from') {
      this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
      this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
      this.isToDatepickerVisible = !this.isToDatepickerVisible;
      this.isFromDatepickerVisible = false;
    }
  }
}
