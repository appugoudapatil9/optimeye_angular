import { EditstakeholderComponent } from './editstakeholder.component';
import { SearchstakeholderComponent } from './searchstakeholder.component';
import { CreatestakeholderComponent } from './createstakeholder.component';
import { StakeholderRoutingModule } from './stakeholder-routing.module';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { StakeholderService } from './stakeholder.service';

@NgModule({
    imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      NgbModule,
      FormsModule,
      DatepickerModule,
      BsDatepickerModule,
      StakeholderRoutingModule,

    ],
    declarations: [
      CreatestakeholderComponent,
      SearchstakeholderComponent,
      EditstakeholderComponent,

    ],
    providers: [StakeholderService]
  })

export class StakeholderModule { }
