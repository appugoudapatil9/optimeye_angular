import { CreatestakeholderComponent } from './createstakeholder.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchstakeholderComponent } from './searchstakeholder.component';
import { EditstakeholderComponent } from './editstakeholder.component';
const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Stakeholder'
      },
      children: [
        {
          path: 'createstakeholder',
          component: CreatestakeholderComponent,
          data: {
            title: 'Create Stakeholder'
          }
         },
        {
          path: 'searchstakeholder',
          component: SearchstakeholderComponent,
          data: {
            title: 'Search Stakeholder'
          }
        },
          {
            path: 'editstakeholder',
            component: EditstakeholderComponent,
            data: {
              title: 'Edit Stakeholder'
            }
        }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class StakeholderRoutingModule { }
