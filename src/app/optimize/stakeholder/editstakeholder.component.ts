import { StakeholderModel } from './stakeholdermodel';
import { ProjectModel } from './../project/projectmodel';
import { LookupModel } from './../../model/lookupmodel';
import { EmployeeModel } from './../project/employeemodel';
import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { StakeholderService } from './stakeholder.service';

import * as moment from 'moment';
import { formatDate } from '@angular/common';

@Component({
    selector: 'app-dashboard',
    templateUrl: './editstakeholder.component.html',
    styleUrls: ['./editstakeholder.component.scss']
})

export class EditstakeholderComponent implements OnInit {

  stakeholdermodel: StakeholderModel = new StakeholderModel();
  submitted = false;
  successMsg;
  errorMsg;
  employeeModels: EmployeeModel[];
  typeData: LookupModel[];
  classificationData: LookupModel[];
  projectModels: ProjectModel[];

    constructor(private route: ActivatedRoute,
        private router: Router,
        private stakeholderService: StakeholderService) { }

    ngOnInit() {
      this.fetchEmployees();
      this.fetchTypes();
      this.fetchClassifications();
      this.fetchProjects();
        this.route.queryParams.subscribe(params => {
            console.log(params);
            this.stakeholderService.editStakeholder(params['idStakeholder']).subscribe(res => {
                this.stakeholdermodel = <StakeholderModel>res;
            });
        });
    }

    fetchEmployees() {
      this.stakeholderService.getAllEmployees()
        .subscribe(
          employeeData => {
            this.employeeModels = <EmployeeModel[]>employeeData;
          },
          );
    }

  fetchTypes() {
      this.stakeholderService.getAllTypes()
      .subscribe(
        types => {
          this.typeData = <LookupModel[]>types;
        },
        );
  }

  fetchClassifications() {
    this.stakeholderService.getAllClassifications()
      .subscribe(
        classifications => {
          this.classificationData = <LookupModel[]>classifications;
        },
        );
  }

  fetchProjects() {
    this.stakeholderService.getAllProjects()
      .subscribe(
        projectsData => {
          this.projectModels = <ProjectModel[]>projectsData;
        },
        );
  }

  save() {
    this.stakeholderService.upadateStakeholders(this.stakeholdermodel)
  .subscribe(data => {
    this.successMsg = data.responseMessage;
  },
  err => {
    this.errorMsg = <any>err;
  },
  () => {
  this.stakeholdermodel = new StakeholderModel();
  });
}

onSubmit() {
this.submitted = true;
this.save();
}
}
