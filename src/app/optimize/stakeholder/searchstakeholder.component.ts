import { StakeholderModel } from './stakeholdermodel';
import { ProjectModel } from '../project/projectmodel';
import { EmployeeModel } from '../project/employeemodel';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StakeholderService } from './stakeholder.service';
import { LookupModel } from '../lookup/lookupmodel';


@Component({
  selector: 'app-dashboard',
  templateUrl: './searchstakeholder.component.html',
  styleUrls: ['./searchstakeholder.component.scss'],
})
export class SearchstakeholderComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  stakeholdermodel: StakeholderModel = new StakeholderModel();
  submitted = false;
  successMsg;
  errorMsg;
  employeeModels: EmployeeModel[];
  typeData: LookupModel[];
  classificationData: LookupModel[];
  projectModels: ProjectModel[];
  stakeholderData: StakeholderModel[];

  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  constructor(private stakeholderService: StakeholderService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
     this.fetchEmployees();
     this.fetchTypes();
     this.fetchClassifications();
     this.fetchProjects();
    }

  fetchEmployees() {
      this.stakeholderService.getAllEmployees()
        .subscribe(
          employeeData => {
            this.employeeModels = <EmployeeModel[]>employeeData;
          },
          );
    }

  fetchTypes() {
      this.stakeholderService.getAllTypes()
      .subscribe(
        types => {
          this.typeData = <LookupModel[]>types;
        },
        );
  }

  fetchClassifications() {
    this.stakeholderService.getAllClassifications()
      .subscribe(
        classifications => {
          this.classificationData = <LookupModel[]>classifications;
        },
        );
  }

  fetchProjects() {
    this.stakeholderService.getAllProjects()
      .subscribe(
        projectsData => {
          this.projectModels = <ProjectModel[]>projectsData;
        },
        );
  }


  search() {
    this.stakeholderService.searchStakeholders(this.stakeholdermodel)
      .subscribe(data => {
        this.stakeholderData = data;
      },
      err => {
      },
      () => {
      this.stakeholdermodel = new StakeholderModel();
      });
  }

  onSubmit() {
    this.submitted = true;
    this.search();
  }

onDateSelectionDone(type): void {
    if (type === 'to') {
        this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
        this.isFromDatepickerVisible = false;
    }
}

toggleCalendar(type: string) {
    if (type === 'from') {
        this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
        this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
        this.isToDatepickerVisible = !this.isToDatepickerVisible;
        this.isFromDatepickerVisible = false;
    }
}

}
