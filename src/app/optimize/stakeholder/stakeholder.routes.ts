import { EditstakeholderComponent } from './editstakeholder.component';
import { SearchstakeholderComponent } from './searchstakeholder.component';
import { Route } from '@angular/router';
import { CreatestakeholderComponent } from './createstakeholder.component';


export const StakeholderRoutes: Route[] = [
    {
        path: 'createstakeholder',
        component: CreatestakeholderComponent
    },
    {
        path: 'searchstakeholder',
        component: SearchstakeholderComponent
    },
    {
        path: 'editstakeholder',
        component: EditstakeholderComponent
    }
];

