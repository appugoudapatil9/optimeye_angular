
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StakeholderService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:8080/stakeholder';

  createStakeholders(stakeholdermodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/createStakeholder`, stakeholdermodel);
  }

  upadateStakeholders(stakeholdermodel: Object): Observable<any> {
    return this.http.put(`${this.baseUrl}` + `/updateStakeholder`, stakeholdermodel);
  }

  searchStakeholders(stakeholdermodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/searchStakeholder`, stakeholdermodel);
  }

  getAllEmployees(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/employeeList`);
  }

  getAllTypes(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/types`);
  }

  getAllClassifications(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/classifications`);
  }

  getAllProjects(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/projects`);
  }

  editStakeholder(id) {
    return this
          .http
          .get(`${this.baseUrl}/${id}`);
    }
}
