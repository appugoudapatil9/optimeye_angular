import { CustomerModel } from './customermodel';
import { CategoryModel } from './categorymodel';
import { ProgramModel } from './programmodel';
import { EmployeeModel } from './employeemodel';
import { ProjectService } from './project.service';
import { ProjectModel } from './projectmodel';
// import { NgbDateCustomParserFormatter } from './dateformat';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
// import { NgbDateFRParserFormatter } from "./ngb-date-fr-parser-formatter";

@Component({
  selector: 'app-dashboard',
  templateUrl: './createproject.component.html',
  styleUrls: ['./createproject.component.scss'],
  })
export class CreateprojectComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  projectmodel: ProjectModel = new ProjectModel();
  projectModels: ProjectModel[] = [];
  submitted = false;
  showmoreflag = false;
  public buttonName: any = 'Additional Info Show';
  successMsg;
  errorMsg;
  investmentModels: EmployeeModel[];
  projectManagerModels: EmployeeModel[];
  programModels: ProgramModel[];
  categoryModels: CategoryModel[];
  functionalManagerModels: EmployeeModel[];
  sponsorModels: EmployeeModel[];
  customerModels: CustomerModel[];

  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  showBtn = -1;
  closeResult: string;
  index = -1;
  year = 0;
  budgetYears: Number[] = [];
  constructor(private projectService: ProjectService, private route: ActivatedRoute,
     private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.budgetyears();
    this.addrows();
    this.fetchInvestmentManager();
    this.fetchProjectManager();
    this.fetchPrograms();
    this.fetchCategory();
    this.fetchFunctionalManager();
    this.fetchAllCustomers();
    this.fetchSponsor();

  }
  open(content, index, projectmodel: ProjectModel) {

    this.projectmodel = projectmodel;
    this.index = index;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }

  addData(modalProjectData: ProjectModel) {
    this.projectmodel = modalProjectData;
    this.projectModels[this.index] = this.projectmodel;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  addrows() {
    for (let i = 0; i < 10; i++) {
      const newItem = new ProjectModel();
      this.projectModels.push(newItem);
    }
  }

  budgetyears() {
    this.year = new Date().getFullYear() + 10 ;
    for (let i = 2000; i < this.year; i++) {
      this.budgetYears.push(i);
    }
  }


  fetchInvestmentManager() {
    this.projectService.getAllInvestmentManager()
      .subscribe(
        investmentManagerData => {
          this.investmentModels = <EmployeeModel[]>investmentManagerData;
        },
      );
  }

  fetchProjectManager() {
    this.projectService.getAllProjectManager()
      .subscribe(
        projectManagerData => {
          this.projectManagerModels = <EmployeeModel[]>projectManagerData;
        },
      );
  }

  fetchPrograms() {
    this.projectService.getAllPrograms()
      .subscribe(
        programModelsData => {
          this.programModels = <ProgramModel[]>programModelsData;
        },
      );
  }

  fetchCategory() {
    this.projectService.getAllCategories()
      .subscribe(
        categoryModelsData => {
          this.categoryModels = <CategoryModel[]>categoryModelsData;
        },
      );
  }

  fetchFunctionalManager() {
    this.projectService.getAllFunctionalManager()
      .subscribe(
        functionalManagerModelsData => {
          this.functionalManagerModels = <EmployeeModel[]>functionalManagerModelsData;
        },
      );
  }

  fetchSponsor() {
    this.projectService.getAllSponsor()
      .subscribe(
        sponsorsData => {
          this.sponsorModels = <EmployeeModel[]>sponsorsData;
         },
        );
       }

  fetchAllCustomers() {
    this.projectService.getAllCustomer()
      .subscribe(
        customerModelsData => {
          this.customerModels = <CustomerModel[]>customerModelsData;
        },
       );
     }

showUndoBtn(index) {
  this.showBtn = index;
}

  save() {
    this.projectService.createProject(this.projectModels)
      .subscribe(data => {
        this.successMsg = data.responseMessage;
      },
      err => {
        this.errorMsg = <any>err;
      },
      () => {
      this.projectmodel = new ProjectModel();
      });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

onDateSelectionDone(type): void {
    if (type === 'to') {
        this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
        this.isFromDatepickerVisible = false;
    }
}

toggleCalendar(type: string) {
    if (type === 'from') {
        this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
        this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
        this.isToDatepickerVisible = !this.isToDatepickerVisible;
        this.isFromDatepickerVisible = false;
    }
}
}
