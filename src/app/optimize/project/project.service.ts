
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:8080/project';

  createProject(projectmodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/createProject`, projectmodel);
  }

  updateProject(projectmodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/updateProject`, projectmodel);
  }

  searchProjects(projectmodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/searchProjects`, projectmodel);
  }

  getAllInvestmentManager(): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/investmentManager`, '');
  }

  getAllProjectManager(): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/projectManager`, '');
  }


  getAllFunctionalManager(): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/functionalManager`, '');
  }

  getAllSponsor(): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/sponsor`, '');
  }

  getAllPrograms(): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/program`, '');
  }

  getAllCategories(): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/category`, '');
  }


  getAllCustomer(): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/customer`, '');
  }

  editProject(id) {
    return this
          .http
          .get(`${this.baseUrl}/${id}`);
    }
}
