import { CustomerModel } from './customermodel';
import { CategoryModel } from './categorymodel';
import { ProgramModel } from './programmodel';
import { EmployeeModel } from './employeemodel';
import { ProjectModel } from './projectmodel';

import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from './project.service';

import * as moment from 'moment';
import { formatDate } from '@angular/common';

@Component({
    selector: 'app-dashboard',
    templateUrl: './editproject.component.html',
    styleUrls: ['./editproject.component.scss']
})

export class EditprojectComponent implements OnInit {

    projectModel: ProjectModel = new ProjectModel();
    investmentModels: EmployeeModel[];
    projectManagerModels: EmployeeModel[];
    programModels: ProgramModel[];
    categoryModels: CategoryModel[];
    functionalManagerModels: EmployeeModel[];
    sponsorModels: EmployeeModel[];
    customerModels: CustomerModel[];
    submitted = false;
    successMsg;
    errorMsg;
    constructor(private route: ActivatedRoute,
        private router: Router,
        private projectService: ProjectService) { }

    ngOnInit() {
    this.fetchInvestmentManager();
    this.fetchProjectManager();
    this.fetchPrograms();
    this.fetchCategory();
    this.fetchFunctionalManager();
    this.fetchAllCustomers();
    this.fetchSponsor();
        this.route.queryParams.subscribe(params => {
            console.log(params);
            this.projectService.editProject(params['projectId']).subscribe(res => {
                this.projectModel = <ProjectModel>res;
                if (this.projectModel.plannedInitDate != null) {
                  this.projectModel.plannedInitDate = new Date(this.projectModel.plannedInitDate);
                }
                if (this.projectModel.plannedFinishDate != null) {
                  this.projectModel.plannedFinishDate = new Date(this.projectModel.plannedFinishDate);
                }
              });
        });
    }

    fetchInvestmentManager() {
        this.projectService.getAllInvestmentManager()
          .subscribe(
            investmentManagerData => {
              this.investmentModels = <EmployeeModel[]>investmentManagerData;
            },
          );
      }

      fetchProjectManager() {
        this.projectService.getAllProjectManager()
          .subscribe(
            projectManagerData => {
              this.projectManagerModels = <EmployeeModel[]>projectManagerData;
            },
          );
      }

      fetchPrograms() {
        this.projectService.getAllPrograms()
          .subscribe(
            programModelsData => {
              this.programModels = <ProgramModel[]>programModelsData;
            },
            );
      }

      fetchCategory() {
        this.projectService.getAllCategories()
          .subscribe(
            categoryModelsData => {
              this.categoryModels = <CategoryModel[]>categoryModelsData;
            },
            );
      }
      fetchFunctionalManager() {
        this.projectService.getAllFunctionalManager()
          .subscribe(
            functionalManagerModelsData => {
              this.functionalManagerModels = <EmployeeModel[]>functionalManagerModelsData;
            },
            );
      }

      fetchSponsor() {
        this.projectService.getAllSponsor()
          .subscribe(
            sponsorsData => {
              this.sponsorModels = <EmployeeModel[]>sponsorsData;
            },
            );
      }

      fetchAllCustomers() {
        this.projectService.getAllCustomer()
          .subscribe(
            customerModelsData => {
              this.customerModels = <CustomerModel[]>customerModelsData;
            },
            );
      }
      save() {
        this.projectService.updateProject(this.projectModel)
      .subscribe(data => {
        this.successMsg = data.responseMessage;
      },
      err => {
        this.errorMsg = <any>err;
      },
      () => {
      this.projectModel = new ProjectModel();
      });
    
    }

    onSubmit() {
    this.submitted = true;
    this.save();
    }
}
