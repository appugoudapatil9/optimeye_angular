import { CustomerModel } from './customermodel';
import { CategoryModel } from './categorymodel';
import { ProgramModel } from './programmodel';
import { EmployeeModel } from './employeemodel';
import { ProjectModel } from './projectmodel';
import { ProjectService } from './project.service';
import { Router } from '@angular/router';
import { OnInit, Component } from '@angular/core';

@Component({
    selector: 'app-dashboard',
    templateUrl: './searchproject.component.html',
    styleUrls: ['./searchproject.component.scss']
  })
  export class SearchprojectComponent implements OnInit {

  projectData: ProjectModel[];
  projectmodel: ProjectModel = new ProjectModel();
  investmentModels: EmployeeModel[];
  projectManagerModels: EmployeeModel[];
  programModels: ProgramModel[];
  categoryModels: CategoryModel[];
  functionalManagerModels: EmployeeModel[];
  sponsorModels: EmployeeModel[];
  customerModels: CustomerModel[];
    constructor(private router: Router, private projectService: ProjectService) {

    }
    ngOnInit() {
      this.fetchInvestmentManager();
      this.fetchProjectManager();
      this.fetchPrograms();
      this.fetchCategory();
      this.fetchFunctionalManager();
      this.fetchAllCustomers();
      this.fetchSponsor();
    }

    fetchInvestmentManager() {
      this.projectService.getAllInvestmentManager()
        .subscribe(
          investmentManagerData => {
            this.investmentModels = <EmployeeModel[]>investmentManagerData;
          },
          );
    }

    fetchProjectManager() {
      this.projectService.getAllProjectManager()
        .subscribe(
          projectManagerData => {
            this.projectManagerModels = <EmployeeModel[]>projectManagerData;
          },
          );
    }
    fetchPrograms() {
      this.projectService.getAllPrograms()
        .subscribe(
          programModelsData => {
            this.programModels = <ProgramModel[]>programModelsData;
          },
          );
    }

    fetchCategory() {
      this.projectService.getAllCategories()
        .subscribe(
          categoryModelsData => {
            this.categoryModels = <CategoryModel[]>categoryModelsData;
          },
          );
    }

    fetchFunctionalManager() {
      this.projectService.getAllFunctionalManager()
        .subscribe(
          functionalManagerModelsData => {
            this.functionalManagerModels = <EmployeeModel[]>functionalManagerModelsData;
          },
          );
    }

    fetchSponsor() {
      this.projectService.getAllSponsor()
        .subscribe(
          sponsorsData => {
            this.sponsorModels = <EmployeeModel[]>sponsorsData;
          },
          );
    }

    fetchAllCustomers() {
      this.projectService.getAllCustomer()
        .subscribe(
          customerModelsData => {
            this.customerModels = <CustomerModel[]>customerModelsData;
          },
          );
    }

    search() {
      this.projectService.searchProjects(this.projectmodel)
        .subscribe(data => {
         this.projectData = data;
        },
        err => {
        },
        () => {
        this.projectmodel = new ProjectModel();
        });
    }

    onSubmit() {

      this.search();
    }

    // editProject(idProject: string): void {
    //   this.router.navigate(['/project/editproject'], {queryParams: {'projectId' : idProject}});
    // }
  }
