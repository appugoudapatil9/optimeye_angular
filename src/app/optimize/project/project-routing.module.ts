import { EditprojectComponent } from './editproject.component';
import { SearchprojectComponent } from './searchproject.component';
import { CreateprojectComponent } from './createproject.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Project'
      },
      children: [
        {
          path: 'createproject',
          component: CreateprojectComponent,
          data: {
            title: 'Add Project'
          }
        },
        {
          path: 'searchproject',
          component: SearchprojectComponent,
          data: {
            title: 'Search Project'
          }
        },
          {
            path: 'editproject',
            component: EditprojectComponent,
            data: {
              title: 'Edit Project'
            }
        },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class ProjectRoutingModule { }
