import { EmployeeModel } from './employeemodel';
import { ProjectModel } from './projectmodel';

export class ProgramModel {

    idProgram: number;
    idProject: number;
    projectName: string;
    idProgramManager: number;
    programManagerName: string;
    programCode: string;
    programName: string;
    programTitle: string;
    description: string;
    programDoc: string;
    budget: number;
    initBudgetYear: string;
    finishBudgetYear: string;
}
