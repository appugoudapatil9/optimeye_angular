
import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditprojectComponent } from './editproject.component';
import { SearchprojectComponent } from './searchproject.component';
import { CreateprojectComponent } from './createproject.component';
import { ProjectService } from './project.service';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';

import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectRoutingModule } from './project-routing.module';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      NgbModule,
      FormsModule,
      DatepickerModule,
      BsDatepickerModule,
      ProjectRoutingModule,

    ],
    declarations: [
      CreateprojectComponent,
      SearchprojectComponent,
      EditprojectComponent
    ],
    providers: [ProjectService, { provide: LOCALE_ID, useValue: 'en-US'}]
  })

export class ProjectModule { }
