import { SearchprojectComponent } from './searchproject.component';
import { CreateprojectComponent } from './createproject.component';
import { Route } from '@angular/router';
import { EditprojectComponent } from './editproject.component';

export const ProjectRoutes: Route[] = [
    {
        path: 'createproject',
        component: CreateprojectComponent
    },
    {
        path: 'searchproject',
        component: SearchprojectComponent
    },
    {
        path: 'editproject',
        component: EditprojectComponent
    }
];

