export class UserModel {

    idUser: number;
    userEmail: string;
    userName: string;
    password: string;
    roleId: any;
    active: string;
    createdBy: number;
    createdName: string;
    updatedBy: number;
    updatedName: string;
    activeFrom: Date;
    activeTo: Date;
    idCompany: number;
    companyName: string;

}
