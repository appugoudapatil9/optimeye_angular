export class RoleModel {

    idRole: number;
    roleName: string;
    active: string;
    createdBy: number;
    createdName: string;
    updatedBy: number;
    updatedName: string;
    activeFrom: Date;
    activeTo: Date;
    idCompany: number;
    companyName: string;

}
