import { EdituserComponent } from './edituser.component';
import { SearchuserComponent } from './searchuser.component';
import { CreateuserComponent } from './createuser.component';


import { Route } from '@angular/router';


export const UserRoutes: Route[] = [
    {
        path: 'createuser',
        component: CreateuserComponent
    },
    {
        path: 'searchuser',
        component: SearchuserComponent
    }
    ,
    {
        path: 'edituser',
        component: EdituserComponent
    }
];

