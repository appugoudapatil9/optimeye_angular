import { CompanyModel } from './../company/companymodel';
import { UserModel } from './usermodel';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { RoleModel } from './rolemodel';

@Component({
    selector: 'app-dashboard',
    templateUrl: './searchuser.component.html',
    styleUrls: ['./searchuser.component.scss'],
    // providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}]
})
export class SearchuserComponent implements OnInit {

    model;
    blsValue = new Date();
    blfValue = new Date();
    submitted = false;
    successMsg;
    errorMsg;

    roleModels: RoleModel[] = [];
    companyModel: CompanyModel = new CompanyModel();
    companyModels: CompanyModel[] = [];
    userModel: UserModel = new UserModel();
    userData: UserModel[] = [];
    isFromDatepickerVisible = false;
    isToDatepickerVisible = false;

    constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
    }
    search() {
        this.userService.searchUsers(this.userModel)
            .subscribe(data => {
                this.userData = data;
            },
                err => {
                    // console.error(err);
                    console.log(err);
                },
                () => {
                    this.userModel = new UserModel();
                });
        // this.customer = new Customer();
    }

    onSubmit() {
        this.submitted = true;
        this.search();
    }

    onDateSelectionDone(type): void {
        if (type === 'to') {
            this.isToDatepickerVisible = false;
        }
        if (type === 'from') {
            this.isFromDatepickerVisible = false;
        }
    }

    toggleCalendar(type: string) {
        if (type === 'from') {
            this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
            this.isToDatepickerVisible = false;
        }
        if (type === 'to') {
            this.isToDatepickerVisible = !this.isToDatepickerVisible;
            this.isFromDatepickerVisible = false;
        }
    }

    editCompany(idUser: string): void {

        this.router.navigate(['/user/edituser'], { queryParams: { 'idUser': idUser } });
    }
}
