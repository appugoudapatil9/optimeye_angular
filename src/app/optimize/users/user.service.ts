
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:8080/userController';
  //  private baseUrl = 'http://3.17.60.48:8080/userController';
  createUsers(userModel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/createUser`, userModel);
  }

  searchUsers(userModel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/searchUser`, userModel);
  }

  searchCompanies(companyModel: Object): Observable<any> {
    // return this.http.post(`http://3.17.60.48:8080/companyController/searchCompanies`, companyModel);

    return this.http.post(`http://localhost:8080/companyController/searchCompanies`, companyModel);
  }

  getAllRoles(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/getRoles`);
  }

  editUser(id) {
    return this
          .http
          .get(`${this.baseUrl}/${id}`);
    }

    updateUser(userModel: Object): Observable<any> {
      return this.http.put(`${this.baseUrl}` + `/updateUser`, userModel);
    }
}








