import { CompanyModel } from './../company/companymodel';
import { RoleModel } from './rolemodel';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './user.service';
import { Component, OnInit } from '@angular/core';
import { UserModel } from './usermodel';

@Component({
  selector: 'app-dashboard',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.scss'],
})
export class CreateuserComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  userModels: UserModel[] = [];
  userModel: UserModel = new UserModel();
  roleModels: RoleModel[] = [];
  companyModel: CompanyModel = new CompanyModel();
  companyModels: CompanyModel[] = [];
  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  submitted = false;
  successMsg;
  errorMsg;
  buttonFlag: Boolean = true;
  closeResult: string;
  index = -1;

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.addrows();
    this.fetchAllRoles();
    this.fetchAllCompanies();
  }

  addrows() {
    for (let i = 0; i < 10; i++) {
      const newItem = new UserModel();
      this.userModels.push(newItem);
    }
  }

  fetchAllRoles() {
    this.userService.getAllRoles()
      .subscribe(
        customerModelsData => {
          this.roleModels = <RoleModel[]>customerModelsData;
        },
      );
  }

  fetchAllCompanies() {
    this.companyModel.active = 'Y';
    this.userService.searchCompanies(this.companyModel)
      .subscribe(
        companyData => {
          this.companyModels = <CompanyModel[]>companyData;
          console.log(this.companyModels);
        },
      );
  }

  save() {
    this.userService.createUsers(this.userModels)
      .subscribe(data => {
        if (data.responseType === 'ERROR') {
          this.errorMsg = data.responseMessage;
        } else {
          this.successMsg = data.responseMessage;
        }
      },
        err => {
          this.errorMsg = <any>err;
        },
        () => {

        });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  onDateSelectionDone(type): void {
    if (type === 'to') {
      this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
      this.isFromDatepickerVisible = false;
    }
  }

  toggleCalendar(type: string) {
    if (type === 'from') {
      this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
      this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
      this.isToDatepickerVisible = !this.isToDatepickerVisible;
      this.isFromDatepickerVisible = false;
    }
  }
}
