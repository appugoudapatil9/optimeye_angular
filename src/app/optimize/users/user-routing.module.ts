import { EdituserComponent } from './edituser.component';
import { SearchuserComponent } from './searchuser.component';
import { CreateuserComponent } from './createuser.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'User'
      },
      children: [
        {
          path: 'createuser',
          component: CreateuserComponent,
          data: {
            title: 'Add User'
          }
        }
        ,
        {
          path: 'searchuser',
          component: SearchuserComponent,
          data: {
            title: 'Search User'
          }
        }
        ,
        {
          path: 'edituser',
          component: EdituserComponent,
          data: {
            title: 'Edit User'
          }
        }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class UserRoutingModule { }
