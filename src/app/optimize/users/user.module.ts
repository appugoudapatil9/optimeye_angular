import { NgModule } from '@angular/core';
import { UserService } from './user.service';
import { UserRoutingModule } from './user-routing.module';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EdituserComponent } from './edituser.component';
import { SearchuserComponent } from './searchuser.component';
import { CreateuserComponent } from './createuser.component';




// registerLocaleData(locale, 'en-US', localeFrExtra);

@NgModule({
    imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      NgbModule,
      FormsModule,
      DatepickerModule,
      BsDatepickerModule,
      UserRoutingModule,

    ],
    declarations: [
      CreateuserComponent,
      SearchuserComponent,
      EdituserComponent,
    ],
    providers: [UserService]
  })

export class UserModule { }
