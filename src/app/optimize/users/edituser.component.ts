import { UserModel } from './usermodel';
import { CompanyModel } from './../company/companymodel';
import { UserService } from './user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleModel } from './rolemodel';


@Component({
    selector: 'app-dashboard',
    templateUrl: './edituser.component.html',
    styleUrls: ['./edituser.component.scss']
})

export class EdituserComponent implements OnInit {

  companyModel: CompanyModel = new CompanyModel();
  submitted = false;
  successMsg;
  errorMsg;
  roleModels: RoleModel[] = [];
  companyModels: CompanyModel[] = [];
  userModel: UserModel = new UserModel();
    constructor(private route: ActivatedRoute,
        private router: Router,
        private userService: UserService) { }

    ngOnInit() {
        this.fetchAllRoles();
        this.fetchAllCompanies();
        this.route.queryParams.subscribe(params => {
            this.userService.editUser(params['idUser']).subscribe(res => {
                this.userModel = <UserModel>res;
                if (this.userModel.activeFrom != null) {
                  this.userModel.activeFrom = new Date(this.userModel.activeFrom);
                }
                if (this.userModel.activeTo != null) {
                    this.userModel.activeTo = new Date(this.userModel.activeTo);
                  }
            });
        });
    }

    fetchAllRoles() {
        this.userService.getAllRoles()
          .subscribe(
            customerModelsData => {
              this.roleModels = <RoleModel[]>customerModelsData;
            },
          );
      }

      fetchAllCompanies() {
        this.companyModel.active = 'Y';
        this.userService.searchCompanies(this.companyModel)
          .subscribe(
            companyModels => {
              this.companyModels = <CompanyModel[]>companyModels;
            },
          );
      }

  save() {
    this.userService.updateUser(this.userModel)
  .subscribe(data => {
    if (data.responseType === 'ERROR') {
      this.errorMsg = data.responseMessage;
    } else {
      this.successMsg = data.responseMessage;
    }
  },
  err => {
    // console.error(err);
    this.errorMsg = <any>err;
    console.log(this.errorMsg);
  },
  () => {
  this.companyModel = new CompanyModel();
  });
// this.customer = new Customer();
}

onSubmit() {
this.submitted = true;
this.save();
}
}
