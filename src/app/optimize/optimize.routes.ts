import { ProgramRoutes } from './programs/program.routes';
import { RiskcateoryRoutes } from './riskcategory/riskcategory.routes';
import { ProjectRoutes } from './project/project.routes';
import { NgModule, Component } from '@angular/core';

import { Route, RouterModule } from '@angular/router';
import { OptimizeComponent } from './optimize.component';
import {LookupRoutes} from './lookup/index';
import { StakeholderRoutes } from './stakeholder/stakeholder.routes';
import { CompanyRoutes } from './company/company.routes';
import { UserRoutes } from './users/user.routes';
import { RuleRoutes } from './rule/rule.routes';
import { AuthGuard } from './shared/guards/auth.guard';
export  const OptimizeRoutes: Route[] = [
    {
        path: 'optimize',
        component: OptimizeComponent,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        children: [
            ...LookupRoutes,
            ...ProjectRoutes,
            ...StakeholderRoutes,
            ...RiskcateoryRoutes,
            ...ProgramRoutes,
            ...CompanyRoutes,
            ...UserRoutes,
            ...RuleRoutes
        ]
    }
];
