import { RuleModule } from './rule/rule.module';
import { UserModule } from './users/user.module';
import { CompanyModule } from './company/company.module';

import { RiskcategoryModule } from './riskcategory/riskcategory.module';
import { ProjectModule } from './project/project.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LookupModule } from './lookup/lookup.module';
import { OptimizeComponent } from './optimize.component';
import { StakeholderModule } from './stakeholder/stakeholder.module';
import { ProgramModule } from './programs/program.module';

@NgModule({

    imports: [
        CommonModule,
        RouterModule,
        LookupModule,
        ProjectModule,
        StakeholderModule,
        RiskcategoryModule,
        ProgramModule,
        CompanyModule,
        UserModule,
        RuleModule
    ],
    declarations: [OptimizeComponent],
    exports: [OptimizeComponent]

})
export class OptimizeModule { }
