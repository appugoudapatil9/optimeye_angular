import { EditcompanyComponent } from './editcompany.component';
import { SearchcompanyComponent } from './searchcompany.component';
import { CreatecompanyComponent } from './createcompany.component';

import { Route } from '@angular/router';


export const CompanyRoutes: Route[] = [
    {
        path: 'company',
        component: SearchcompanyComponent
    },
    {
        path: 'createcompany',
        component: CreatecompanyComponent
    },
    {
        path: 'searchcompany',
        component: SearchcompanyComponent
    }
    ,
    {
        path: 'editcompany',
        component: EditcompanyComponent
    }
];

