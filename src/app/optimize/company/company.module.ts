import { SearchcompanyComponent } from './searchcompany.component';
import { CreatecompanyComponent } from './createcompany.component';
import { CompanyRoutingModule } from './company-routing.module';

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CompanyService } from './company.service';
import { EditcompanyComponent } from './editcompany.component';



// registerLocaleData(locale, 'en-US', localeFrExtra);

@NgModule({
    imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      NgbModule,
      FormsModule,
      DatepickerModule,
      BsDatepickerModule,
      CompanyRoutingModule,

    ],
    declarations: [
      CreatecompanyComponent,
      SearchcompanyComponent,
      EditcompanyComponent,
    ],
    providers: [CompanyService]
  })

export class CompanyModule { }
