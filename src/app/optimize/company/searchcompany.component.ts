import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CompanyService } from './company.service';
import { CompanyModel } from './companymodel';



@Component({
  selector: 'app-dashboard',
  templateUrl: './searchcompany.component.html',
  styleUrls: ['./searchcompany.component.scss'],
  // providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}]
})
export class SearchcompanyComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  companyModel: CompanyModel = new CompanyModel();
  companyData: CompanyModel[] = [];
  submitted = false;
  successMsg;
  errorMsg;

  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  constructor(private companyservice: CompanyService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    }
 search() {
    this.companyservice.searchCompanies(this.companyModel)
      .subscribe(data => {
        this.companyData = data;
      },
      err => {
        // console.error(err);
        console.log(err);
      },
      () => {
      this.companyModel = new CompanyModel();
      });
    // this.customer = new Customer();
  }

  onSubmit() {
    this.submitted = true;
    this.search();
  }

onDateSelectionDone(type): void {
    if (type === 'to') {
        this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
        this.isFromDatepickerVisible = false;
    }
}

toggleCalendar(type: string) {
    if (type === 'from') {
        this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
        this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
        this.isToDatepickerVisible = !this.isToDatepickerVisible;
        this.isFromDatepickerVisible = false;
    }
}

editCompany(idCompany: string): void {

  this.router.navigate(['/company/editcompany'], {queryParams: {'idCompany' : idCompany}});
}
}
