
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  // private baseUrl = 'http://localhost:8080/companyController';
  private baseUrl = 'http://3.17.60.48:8080/companyController';

  createCompanies(companymodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/createCompanies`, companymodel);
  }

  searchCompanies(companymodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/searchCompanies`, companymodel);
  }

  editCompany(id) {
    return this
          .http
          .get(`${this.baseUrl}/${id}`);
    }

    updateCompany(companyModel: Object): Observable<any> {
      return this.http.put(`${this.baseUrl}` + `/updateCompanies`, companyModel);
    }
}








