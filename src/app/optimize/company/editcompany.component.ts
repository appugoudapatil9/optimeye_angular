import { CompanyModel } from './companymodel';
import { CompanyService } from './company.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'app-dashboard',
    templateUrl: './editcompany.component.html',
    styleUrls: ['./editcompany.component.scss']
})

export class EditcompanyComponent implements OnInit {

  companyModel: CompanyModel = new CompanyModel();
  submitted = false;
  successMsg;
  errorMsg;


    constructor(private route: ActivatedRoute,
        private router: Router,
        private companyService: CompanyService) { }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.companyService.editCompany(params['idCompany']).subscribe(res => {
                this.companyModel = <CompanyModel>res;
                if (this.companyModel.activeFrom != null) {
                  this.companyModel.activeFrom = new Date(this.companyModel.activeFrom);
                }
                if (this.companyModel.activeTo != null) {
                    this.companyModel.activeTo = new Date(this.companyModel.activeTo);
                  }
            });
        });
    }

  save() {
    this.companyService.updateCompany(this.companyModel)
  .subscribe(data => {
    if (data.responseCode === '200') {
      this.successMsg = data.responseMessage;
    } else {
      this.errorMsg = data.responseMessage;
    }
  },
  err => {
    // console.error(err);
    this.errorMsg = <any>err;
    console.log(this.errorMsg);
  },
  () => {
  // this.companyModel = new CompanyModel();
  });
// this.customer = new Customer();
}

onSubmit() {
this.submitted = true;
this.save();
}
}
