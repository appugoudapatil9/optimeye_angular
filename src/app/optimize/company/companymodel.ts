export class CompanyModel {

    idCompany: number;
    name: string;
    active: string;
    createdBy: number;
    createdName: string;
    updatedBy: number;
    updatedName: string;
    activeFrom: Date;
    activeTo: Date;
}
