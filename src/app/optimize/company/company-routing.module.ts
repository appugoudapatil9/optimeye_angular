import { EditcompanyComponent } from './editcompany.component';
import { SearchcompanyComponent } from './searchcompany.component';
import { CreatecompanyComponent } from './createcompany.component';

import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Company'
      },
      children: [
        {
          path: 'createcompany',
          component: CreatecompanyComponent,
          data: {
            title: 'Add Company'
          }
        }
        ,
        {
          path: 'searchcompany',
          component: SearchcompanyComponent,
          data: {
            title: 'Search Company'
          }
        }
        ,
        {
          path: 'editcompany',
          component: EditcompanyComponent,
          data: {
            title: 'Edit Company'
          }
        }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class CompanyRoutingModule { }
