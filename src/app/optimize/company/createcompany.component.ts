import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from './company.service';
import { Component, OnInit } from '@angular/core';
import { CompanyModel } from './companymodel';



@Component({
  selector: 'app-dashboard',
  templateUrl: './createcompany.component.html',
  styleUrls: ['./createcompany.component.scss'],
  // providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}]
})
export class CreatecompanyComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  companyModels: CompanyModel[] = [];
  companyModel: CompanyModel = new CompanyModel();

  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  submitted = false;
  successMsg;
  errorMsg;
  buttonFlag: Boolean = true;
  closeResult: string;
  index = -1;

  constructor(private companyService: CompanyService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.addrows();
  }

  addrows() {
    for (let i = 0; i < 10; i++) {
      const newItem = new CompanyModel();
      this.companyModels.push(newItem);
    }
  }

  save() {
    this.companyService.createCompanies(this.companyModels)
      .subscribe(data => {
        if (data.responseType === 'ERROR') {
          this.errorMsg = data.responseMessage;
        } else {
          this.successMsg = data.responseMessage;
        }
      },
      err => {
        this.errorMsg = <any>err;
      },
      () => {

      });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

onDateSelectionDone(type): void {
    if (type === 'to') {
        this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
        this.isFromDatepickerVisible = false;
    }
}

toggleCalendar(type: string) {
    if (type === 'from') {
        this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
        this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
        this.isToDatepickerVisible = !this.isToDatepickerVisible;
        this.isFromDatepickerVisible = false;
    }
}
}
