import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions, RequestOptionsArgs, RequestMethod, Request, URLSearchParams } from '@angular/http';
import '../../rxjs-operators';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from '../loader/loader.service';
import { Observer } from 'rxjs/Observer';
import { map } from 'rxjs/operators';

export enum Action { QueryStart, QueryStop };

@Injectable()
export class HttpBaseService {
    process: EventEmitter<any> = new EventEmitter<any>();
    authFailed: EventEmitter<any> = new EventEmitter<any>();
    public shouldShowLoader = true;

    constructor(private _http: Http, private loader: LoaderService, private router: Router) {
    }

    public get<T>(url: string, params?: URLSearchParams, options?: RequestOptionsArgs): Observable<T> {
        return this._request<T>(RequestMethod.Get, url, null, params, options);
    }

    public post<T>(url: string, body: string, options?: RequestOptionsArgs): Observable<T> {
        return this._request<T>(RequestMethod.Post, url, body, null, options);
    }

    public put<T>(url: string, body: string, options?: RequestOptionsArgs): Observable<T> {
        return this._request<T>(RequestMethod.Put, url, body, null, options);
    }

    public delete<T>(url: string, options?: RequestOptionsArgs): Observable<T> {
        return this._request<T>(RequestMethod.Delete, url, null, null, options);
    }

    private showLoader() {
        if (this.shouldShowLoader) {
            this.loader.isLoading = true;
        }
    }

    private hideLoader() {
        if (this.shouldShowLoader) {
            this.loader.isLoading = false;
        }
    }

    private _buildAuthHeader(): string {
        if (sessionStorage.getItem('token') !== null) {
            return 'Bearer ' + sessionStorage.getItem('token');
        }
        return '';
    }

    private _request<T>(method: RequestMethod, url: string, body?: string,
        params?: URLSearchParams, options?: RequestOptionsArgs): Observable<T> {
        if (options) {
            delete options.method;
            delete options.params;
            delete options.url;
            delete options.body;
        }
        const requestOptions = new RequestOptions(Object.assign({
            method: method,
            url: url,
            body: body,
            search: params
        }, options));
        requestOptions.search = params;
        if (!requestOptions.headers) {
            requestOptions.headers = new Headers();
        }
        if (!requestOptions.headers.get('Content-Type')) {
            requestOptions.headers.set('Content-Type', 'application/json');
        }
        const authHeader = this._buildAuthHeader();
        if (authHeader.length > 0 && !requestOptions.headers.get('Authorization')) {
            requestOptions.headers.set('Authorization', authHeader);
        }
        this.showLoader();
        return Observable.create((observer: Observer<T>) => {
            this.process.next(Action.QueryStart);
            this._http.request(new Request(requestOptions)).pipe(
                map(res => res.json() as T))
                .subscribe(
                (res) => {
                    observer.next(res);
                    observer.complete();
                },
                (err) => {
                    this.hideLoader();
                    switch (err.status) {
                        case 401:
                            if (this.router.url !== '/') {
                                // intercept 401
                                observer.complete();
                                this.router.navigate(['/']);
                            }
                            break;
                        default:
                            break;
                    }
                    observer.error(err);
                    observer.complete();
                },
                () => {
                    this.hideLoader();
                    this.process.next(Action.QueryStop);
                });
        });
    }
}
