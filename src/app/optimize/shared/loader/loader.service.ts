import { Injectable } from '@angular/core';

export interface ILoader {
    isLoading: boolean;
}

@Injectable()
export class LoaderService implements ILoader {
    public isLoading = false;
}
