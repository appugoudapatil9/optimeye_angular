import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // Check to see if a user has a valid JWT
    if (sessionStorage.getItem('token') !== null) {
      // If they do, return true and allow the user to load the home component
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // Check to see if a user has a valid JWT
    if (sessionStorage.getItem('token') !== null) {
      // If they do, return true and allow the user to load the home component
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
