import { EditruleComponent } from './editrule.component';
import { SearchruleComponent } from './searchrule.component';
import { CreateruleComponent } from './createrule.component';

import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Rule'
      },
      children: [
        {
          path: 'createrule',
          component: CreateruleComponent,
          data: {
            title: 'Add Rule'
          }
        }
        ,
        {
          path: 'searchrule',
          component: SearchruleComponent,
          data: {
            title: 'Search Rule'
          }
        }
        ,
        {
          path: 'editrule',
          component: EditruleComponent,
          data: {
            title: 'Edit Rule'
          }
        }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class RuleRoutingModule { }
