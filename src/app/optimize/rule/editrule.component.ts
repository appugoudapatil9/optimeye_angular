import { LookupModel } from './../../model/lookupmodel';
import { RuleRiskDetailsModel } from './ruleRiskDetailsModel';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RuleModel } from './rulemodel';
import { RuleService } from './rule.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './editrule.component.html',
  styleUrls: ['./editrule.component.scss']
})

export class EditruleComponent implements OnInit {

  ruleModel: RuleModel = new RuleModel();
  submitted = false;
  successMsg;
  errorMsg;
  lookupModels: LookupModel[] = [];
  ruleRiskDetails: RuleRiskDetailsModel[] = [];
  constructor(private route: ActivatedRoute,
    private router: Router,
    private ruleService: RuleService) { }

  ngOnInit() {
    this.fetchRiskType();
    this.route.queryParams.subscribe(params => {
      this.ruleService.editRule(params['idRule']).subscribe(res => {
        this.ruleModel = <RuleModel>res;
        this.ruleRiskDetails = this.ruleModel.ruleRiskDetails;
      });
    });
  }

  riskDetailsRows() {
    const newItem = new RuleRiskDetailsModel();
    this.ruleRiskDetails.push(newItem);
}

  fetchRiskType() {
    this.ruleService.getAllRiskTypes()
      .subscribe(
        lookupData => {
          this.lookupModels = <LookupModel[]>lookupData;
        },
      );
  }

  save() {
    this.ruleModel.ruleRiskDetails = this.ruleRiskDetails;
    this.ruleService.updateRule(this.ruleModel)
      .subscribe(data => {
        if (data.responseCode === '200') {
          this.successMsg = data.responseMessage;
        } else {
          this.errorMsg = data.responseMessage;
        }
      },
        err => {
          // console.error(err);
          this.errorMsg = <any>err;
          console.log(this.errorMsg);
        },
        () => {
          // this.ruleModel = new RuleModel();
        });
    // this.customer = new Customer();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
