import { LookupModel } from './../lookup/lookupmodel';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { RuleRiskDetailsModel } from './ruleRiskDetailsModel';
import { ActivatedRoute, Router } from '@angular/router';
import { RuleService } from './rule.service';
import { RuleModel } from './rulemodel';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-dashboard',
  templateUrl: './searchrule.component.html',
  styleUrls: ['./searchrule.component.scss'],
  // providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}]
})
export class SearchruleComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  ruleModel: RuleModel = new RuleModel();
  rulesData: RuleModel[] = [];
  ruleRiskDetails: RuleRiskDetailsModel[] = [];
  lookupModels: LookupModel[] = [];
  submitted = false;
  successMsg;
  errorMsg;
  riskType: string;
  closeResult: string;
  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  constructor(private ruleService: RuleService, private route: ActivatedRoute,
    private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.fetchRiskType();
  }

  fetchRiskType() {
    this.ruleService.getAllRiskTypes()
      .subscribe(
        lookupData => {
          this.lookupModels = <LookupModel[]>lookupData;
        },
      );
  }
  search() {
    this.riskType = this.ruleModel.riskType;
    this.ruleService.searchRule(this.ruleModel)
      .subscribe(data => {
        this.rulesData = data;
      },
        err => {
          console.log(err);
        },
        () => {
          this.ruleModel = new RuleModel();
        });
  }

  onSubmit() {
    this.submitted = true;
    this.search();
  }

  onDateSelectionDone(type): void {
    if (type === 'to') {
      this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
      this.isFromDatepickerVisible = false;
    }
  }

  toggleCalendar(type: string) {
    if (type === 'from') {
      this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
      this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
      this.isToDatepickerVisible = !this.isToDatepickerVisible;
      this.isFromDatepickerVisible = false;
    }
  }

  editRule(idRule: string): void {
    this.router.navigate(['/rule/editrule'], { queryParams: { 'idRule': idRule } });
  }

  getRiskDetails(content, idRule: number) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.ruleModel.idRule = idRule;
    this.ruleModel.riskType = this.riskType;
    this.ruleService.getAllRiskDetailsByRuleId(this.ruleModel)
      .subscribe(data => {
        this.ruleRiskDetails = data;
      },
        err => {
          console.log(err);
        },
        () => {

        });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
