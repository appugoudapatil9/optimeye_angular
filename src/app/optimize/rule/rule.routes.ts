import { EditruleComponent } from './editrule.component';
import { SearchruleComponent } from './searchrule.component';
import { CreateruleComponent } from './createrule.component';

import { Route } from '@angular/router';


export const RuleRoutes: Route[] = [
    {
        path: 'createrule',
        component: CreateruleComponent
    },
    {
        path: 'searchrule',
        component: SearchruleComponent
    }
    ,
    {
        path: 'editrule',
        component: EditruleComponent
    }
];

