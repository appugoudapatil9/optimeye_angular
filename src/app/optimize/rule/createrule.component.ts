import { LookupModel } from './../lookup/lookupmodel';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { RuleRiskDetailsModel } from './ruleRiskDetailsModel';
import { ActivatedRoute, Router } from '@angular/router';
import { RuleService } from './rule.service';
import { Component, OnInit } from '@angular/core';
import { RuleModel } from './rulemodel';

@Component({
  selector: 'app-dashboard',
  templateUrl: './createrule.component.html',
  styleUrls: ['./createrule.component.scss'],
})
export class CreateruleComponent implements OnInit {

  model;
  blsValue = new Date();
  blfValue = new Date();
  ruleModels: RuleModel[] = [];
  ruleModel: RuleModel = new RuleModel();
  ruleRiskDetail: RuleRiskDetailsModel = new RuleRiskDetailsModel();
  ruleRiskDetails: RuleRiskDetailsModel[] = [];
  lookupModels: LookupModel[] = [];
  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  submitted = false;
  successMsg;
  errorMsg;
  buttonFlag: Boolean = true;
  closeResult: string;
  index = -1;

  constructor(private ruleService: RuleService, private route: ActivatedRoute,
    private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.addrows();
    this.fetchRiskType();
    }

  fetchRiskType() {
    this.ruleService.getAllRiskTypes()
      .subscribe(
        lookupData => {
          this.lookupModels = <LookupModel[]> lookupData;
        },
      );
  }

  addRiskDetailsRows() {
    const newItem = new RuleRiskDetailsModel();
    this.ruleRiskDetails.push(newItem);
  }

  riskDetailsRows(addrow =  false, index = 0) {
    const newItem = new RuleRiskDetailsModel();
      const a = this.ruleModels[index].ruleRiskDetails;
      if (a) {
        this.ruleRiskDetails = this.ruleModels[index].ruleRiskDetails;
        return null;
      } else if (a === undefined) {
        this.ruleRiskDetails = [];
        this.ruleRiskDetails.push(newItem);
      }
 }

  addrows() {

    for (let i = 0; i < 10; i++) {
      const newItem = new RuleModel();
      this.ruleModels.push(newItem);
    }
  }

  open(content, index, ruleModel: RuleModel) {
    this.riskDetailsRows(false, index);
    this.ruleModel = ruleModel;
    this.index = index;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  addData(ruleRiskDetail: RuleRiskDetailsModel[]) {

      console.log('ruleRiskDetail', ruleRiskDetail );
      this.ruleModel.ruleRiskDetails = ruleRiskDetail;
      this.ruleModels[this.index] = this.ruleModel;
      console.log('ruleModel', this.ruleModels);
  }

  save() {
    console.log('submit rule', this.ruleModels);
    this.ruleService.createRules(this.ruleModels)
      .subscribe(data => {
        if (data.responseType === 'ERROR') {
          this.errorMsg = data.responseMessage;
        } else {
          this.successMsg = data.responseMessage;
        }
      },
        err => {
          this.errorMsg = <any>err;
        },
        () => {
        });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  onDateSelectionDone(type): void {
    if (type === 'to') {
      this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
      this.isFromDatepickerVisible = false;
    }
  }

  toggleCalendar(type: string) {
    if (type === 'from') {
      this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
      this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
      this.isToDatepickerVisible = !this.isToDatepickerVisible;
      this.isFromDatepickerVisible = false;
    }
  }
}
