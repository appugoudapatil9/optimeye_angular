export class RuleRiskDetailsModel {

    idRule: number;

    idRuleRiskDetails: number;

    defaultRisk: string;

    defaultWeightage: string;

    activeFlag: string;

    riskType: string;

}
