
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RuleService {

  constructor(private http: HttpClient) { }

  // private baseUrl = 'http://localhost:8080/ruleController';
  private baseUrl = 'http://3.17.60.48:8080/ruleController';

  createRules(ruleModel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/createRule`, ruleModel);
  }

  searchRule(ruleModel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/searchRule`, ruleModel);
  }

  editRule(id) {
    return this
      .http
      .get(`${this.baseUrl}/${id}`);
  }

  updateRule(ruleModel: Object): Observable<any> {
    return this.http.put(`${this.baseUrl}` + `/updateRule`, ruleModel);
  }

  getAllRiskScores(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/getRiskScores`);
  }

  ruleGroupedScores(ruleModel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/ruleGroupedScores`, ruleModel);
  }

  getAllRiskTypes(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/riskTypes`);
  }

  getAllRiskDetailsByRuleId(ruleModel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/riskDetails`, ruleModel);
  }

}








