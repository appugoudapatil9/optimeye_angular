import { RuleRiskDetailsModel } from './ruleRiskDetailsModel';
export class RuleModel {
    idRule: number;
    idCompany: number;
    companyName: string;
    ruleSource: string;
    ruleName: string;
    ruleGrouping: string;
    defaultRisk: string;
    activeFlag: string;
    defaultWeightage: string;
    manualAutoFlag: string;
    system: string;
    enterpriseRisk: string;
    internalAudit: string;
    complaince: string;
    ruleRiskDetails: RuleRiskDetailsModel [];
    riskType: string;
}
