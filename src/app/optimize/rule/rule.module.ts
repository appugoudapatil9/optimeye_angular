import { RuleService } from './rule.service';
import { EditruleComponent } from './editrule.component';
import { SearchruleComponent } from './searchrule.component';
import { CreateruleComponent } from './createrule.component';
import { RuleRoutingModule } from './rule-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      NgbModule,
      FormsModule,
      DatepickerModule,
      BsDatepickerModule,
      RuleRoutingModule,

    ],
    declarations: [
      CreateruleComponent,
      SearchruleComponent,
      EditruleComponent,
    ],
    providers: [RuleService]
  })

export class RuleModule { }
