
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:8080/program';

  createProgram(programmodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/createProgram`, programmodel);
  }

  getAllProgramManagers(): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/programManager`, '');
  }

  updateProgram(programmodel: Object): Observable<any> {
    return this.http.put(`${this.baseUrl}` + `/updateProgram`, programmodel);
  }

  searchPrograms(programmodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/searchPrograms`, programmodel);
  }

  editProgram(id) {
    return this
          .http
          .get(`${this.baseUrl}/${id}`);
    }
}
