import { ProgramModel } from './../project/programmodel';
import { EmployeeModel } from '../project/employeemodel';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProgramService } from './program.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-dashboard',
  templateUrl: './createprogram.component.html',
  styleUrls: ['./createprogram.component.scss'],
})
export class CreateprogramComponent implements OnInit {

  model;

  programmodel: ProgramModel = new ProgramModel();
  programmodels: ProgramModel[] = [];
  submitted = false;
  successMsg;
  errorMsg;
  employeeModels: EmployeeModel[];

  closeResult: string;
  index = -1;

  isFromDatepickerVisible = false;
  isToDatepickerVisible = false;

  constructor(private programService: ProgramService, private route: ActivatedRoute,
    private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.addrows();
    this.fetchProgramManagers();
  }

  addrows() {
    for (let i = 0; i < 10; i++) {
      const newItem = new ProgramModel();
      this.programmodels.push(newItem);
    }
  }

  open(content, index, programmodel: ProgramModel) {

    this.programmodel = programmodel;
    this.index = index;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  addData(modalProgramData: ProgramModel) {
    this.programmodel = modalProgramData;
    this.programmodels[this.index] = this.programmodel;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  fetchProgramManagers() {
    this.programService.getAllProgramManagers()
      .subscribe(
        programManagerData => {
          this.employeeModels = <EmployeeModel[]>programManagerData;
        },
      );
  }

  save() {
    this.programService.createProgram(this.programmodels)
      .subscribe(data => {
        this.successMsg = data.responseMessage;
      },
        err => {
          this.errorMsg = <any>err;
        },
        () => {
          this.programmodel = new ProgramModel();
        });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  onDateSelectionDone(type): void {
    if (type === 'to') {
      this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
      this.isFromDatepickerVisible = false;
    }
  }

  toggleCalendar(type: string) {
    if (type === 'from') {
      this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
      this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
      this.isToDatepickerVisible = !this.isToDatepickerVisible;
      this.isFromDatepickerVisible = false;
    }
  }
}
