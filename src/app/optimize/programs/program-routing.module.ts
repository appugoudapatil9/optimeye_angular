import { EditprogramComponent } from './editprogram.component';
import { CreateprogramComponent } from './createprogram.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchprogramComponent } from './searchprogram.component';

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Programs'
      },
      children: [
        {
          path: 'createprogram',
          component: CreateprogramComponent,
          data: {
            title: 'Create Program'
          }
         },
        {
          path: 'searchprogram',
          component: SearchprogramComponent,
          data: {
            title: 'Search Program'
          }
        }
        ,
          {
            path: 'editprogram',
            component: EditprogramComponent,
            data: {
              title: 'Edit Program'
            }
        }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class ProgramRoutingModule { }
