export class StakeholderModel {
    idStakeholder: number;
    idProject: number;
    projectName: string;
    idEmployee: number;
    employeeName: string;
    type: number;
    typeName: string;
    classification: number;
    classificationName: string;
    projectRole: string;
    requirements: string;
    expectations: string;
    influence: string;
    mgmtStratergy: string;
    contactName: string;
    department: string;
    orderToShow: number;
    comments: string;
}
