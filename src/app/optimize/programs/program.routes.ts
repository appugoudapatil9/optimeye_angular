import { EditprogramComponent } from './editprogram.component';

import { Route } from '@angular/router';
import { CreateprogramComponent } from './createprogram.component';
import { SearchprogramComponent } from './searchprogram.component';


export const ProgramRoutes: Route[] = [
    {
        path: 'createprogram',
        component: CreateprogramComponent
    },
    {
        path: 'searchprogram',
        component: SearchprogramComponent
    },
    {
        path: 'editprogram',
        component: EditprogramComponent
    }
];

