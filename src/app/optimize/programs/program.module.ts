import { EditprogramComponent } from './editprogram.component';
import { ProgramService } from './program.service';
import { NgModule } from '@angular/core';
import { CreateprogramComponent } from './createprogram.component';
import { FormsModule } from '@angular/forms';
import { ProgramRoutingModule } from './program-routing.module';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';

import { CommonModule } from '@angular/common';
import { SearchprogramComponent } from './searchprogram.component';


@NgModule({
    imports: [
      CommonModule,
      HttpClientModule,
      NgbModule,
      FormsModule,
      DatepickerModule,
      BsDatepickerModule,
      ProgramRoutingModule,

    ],
    declarations: [
      CreateprogramComponent,
      SearchprogramComponent,
      EditprogramComponent,

    ],
    providers: [ProgramService]
  })

export class ProgramModule { }
