import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeModel } from './../project/employeemodel';
import { Component, OnInit } from '@angular/core';
import { ProgramModel } from '../project/programmodel';
import { ProgramService } from './program.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-dashboard',
  templateUrl: './searchprogram.component.html',
  styleUrls: ['./searchprogram.component.scss'],
})
export class SearchprogramComponent implements OnInit {

    model;

    programmodel: ProgramModel = new ProgramModel();
    programmodels: ProgramModel[] = [];
    submitted = false;
    successMsg;
    errorMsg;
    employeeModels: EmployeeModel[];

    closeResult: string;
    index = -1;

    isFromDatepickerVisible = false;
    isToDatepickerVisible = false;

    constructor(private programService: ProgramService, private route: ActivatedRoute,
        private router: Router, private modalService: NgbModal) { }

      ngOnInit() {
        this.fetchProgramManagers();
      }

      fetchProgramManagers() {
        this.programService.getAllProgramManagers()
          .subscribe(
            programManagerData => {
              this.employeeModels = <EmployeeModel[]>programManagerData;
            },
          );
      }


  search() {
    this.programService.searchPrograms(this.programmodel)
      .subscribe(data => {
        this.programmodels = data;
      },
      err => {
      },
      () => {
      this.programmodel = new ProgramModel();
      });
  }

  onSubmit() {
    this.submitted = true;
     this.search();
  }

onDateSelectionDone(type): void {
    if (type === 'to') {
        this.isToDatepickerVisible = false;
    }
    if (type === 'from') {
        this.isFromDatepickerVisible = false;
    }
}

toggleCalendar(type: string) {
    if (type === 'from') {
        this.isFromDatepickerVisible = !this.isFromDatepickerVisible;
        this.isToDatepickerVisible = false;
    }
    if (type === 'to') {
        this.isToDatepickerVisible = !this.isToDatepickerVisible;
        this.isFromDatepickerVisible = false;
    }
}

}
