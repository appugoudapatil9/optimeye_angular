import { ProgramModel } from './../project/programmodel';
import { EmployeeModel } from '../project/employeemodel';
import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { ProgramService } from './program.service';

import * as moment from 'moment';
import { formatDate } from '@angular/common';

@Component({
    selector: 'app-dashboard',
    templateUrl: './editprogram.component.html',
    styleUrls: ['./editprogram.component.scss']
})

export class EditprogramComponent implements OnInit {

  programmodel: ProgramModel = new ProgramModel();
  submitted = false;
  successMsg;
  errorMsg;
  employeeModels: EmployeeModel[];

    constructor(private route: ActivatedRoute,
        private router: Router,
        private programService: ProgramService) { }

    ngOnInit() {
      this.fetchEmployees();

        this.route.queryParams.subscribe(params => {
            console.log(params);
            this.programService.editProgram(params['idProgram']).subscribe(res => {
                this.programmodel = <ProgramModel>res;
            });
        });
    }

    fetchEmployees() {
      this.programService.getAllProgramManagers()
        .subscribe(
          employeeData => {
            this.employeeModels = <EmployeeModel[]>employeeData;
          },
          );
    }

  save() {
    this.programService.updateProgram(this.programmodel)
  .subscribe(data => {
    this.successMsg = data.responseMessage;
  },
  err => {
    this.errorMsg = <any>err;
  },
  () => {
  this.programmodel = new ProgramModel();
  });
}

onSubmit() {
this.submitted = true;
this.save();
}
}
