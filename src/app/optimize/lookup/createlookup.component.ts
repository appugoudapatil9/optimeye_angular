
import { LookupService } from './lookup.service';
import { Component, OnInit } from '@angular/core';
import { LookupModel } from './lookupmodel';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './createlookup.component.html',
  styleUrls: ['./createlookup.component.scss']
})
export class CreatelookupComponent implements OnInit {

  lookupmodel: LookupModel = new LookupModel();
  lookups: LookupModel[] = [];
  submitted = false;
  successMsg;
  errorMsg;
  buttonFlag: Boolean = true;
  closeResult: string;
  index = -1;

  constructor(private lookupService: LookupService, private route: ActivatedRoute,
     private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.addrows();
  }

  addrows() {
    for (let i = 0; i < 10; i++) {
      const newItem = new LookupModel();
      this.lookups.push(newItem);
    }
  }

  open(content, index, lookupmodel: LookupModel) {

    this.lookupmodel = lookupmodel;
    this.index = index;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  addData(modalLookupData: LookupModel) {
    this.lookupmodel = modalLookupData;
    this.lookups[this.index] = this.lookupmodel;
  }

  save() {
    this.lookupService.createLookup(this.lookups)
      .subscribe(data => {
        if (data.responseType === 'ERROR') {
          this.errorMsg = data.responseMessage;
        } else {
          this.successMsg = data.responseMessage;
        }
      },
      err => {
        this.errorMsg = <any>err;
      },
      () => {

      });
  }

  addNewItem() {
    const newItem = new LookupModel();
    this.lookups.push(newItem);
    this.buttonFlag = false;
}

canDelete(i: number): boolean {
  return this.lookups.length > 1;
}

deleteItem(i: number): void {
  if (confirm('Are you sure you want to remove this?')) {
    this.lookups.splice(i, 1);
    if ( this.lookups.length === 1 ) {
      this.buttonFlag = true;
    }
  }
}

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
