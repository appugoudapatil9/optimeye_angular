import { LookupModel } from './lookupmodel';
import { LookupService } from './lookup.service';
import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './editlookup.component.html',
    styleUrls: ['./editlookup.component.scss']
})

export class EditlookupComponent implements OnInit {

    lookupmodel: LookupModel = new LookupModel();
    submitted = false;
    successMsg;
    errorMsg;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private lookupService: LookupService) { }
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.lookupService.editLookup(params['lookupId']).subscribe(res => {
                this.lookupmodel = <LookupModel>res;
                if (this.lookupmodel.startDate != null) {
                  this.lookupmodel.startDate = new Date(this.lookupmodel.startDate);
                }
                if (this.lookupmodel.endDate != null) {
                    this.lookupmodel.endDate = new Date(this.lookupmodel.endDate);
                  }
            });
        });
    }

    save() {
        this.lookupService.updateLookup(this.lookupmodel)
      .subscribe(data => {
        if (data.responseCode === '200') {
            this.successMsg = data.responseMessage;
          } else {
            this.errorMsg = data.responseMessage;
          }
      },
      err => {
        this.errorMsg = <any>err;
      },
      () => {
      // this.lookupmodel = new LookupModel();
      });

    }

    onSubmit() {
    this.submitted = true;
    this.save();
    }
}
