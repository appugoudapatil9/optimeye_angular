import { Router } from '@angular/router';
import { LookupModel } from './lookupmodel';
import { LookupService } from './lookup.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './searchlookup.component.html',
  styleUrls: ['./searchlookup.component.scss']
})
export class SearchlookupComponent implements OnInit {

  lookups: LookupModel[];
  lookupmodel: LookupModel = new LookupModel();
  constructor(private router: Router, private lookupService: LookupService) { }

  ngOnInit() {

  }

  // editLookup(idLookup: string): void {

  //   this.router.navigate(['/lookup/editlookup'], {queryParams: {'lookupId' : idLookup}});
  // }

  search() {
    this.lookupService.searchLookup(this.lookupmodel)
      .subscribe(data => {
        this.lookups = data;
      },
      err => {
      },
      () => {
        this.lookupmodel = new LookupModel();
      });

  }

  onSubmit() {
    this.search();
  }
}
