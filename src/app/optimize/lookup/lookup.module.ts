import { EditlookupComponent } from './editlookup.component';
import { SearchlookupComponent } from './searchlookup.component';
import { LookupService } from './lookup.service';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LookupRoutingModule } from './lookup-routing.module';
import { CreatelookupComponent } from './createlookup.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    imports: [
      CommonModule,
      ReactiveFormsModule,
      HttpClientModule,
      FormsModule,
      DatepickerModule,
      BsDatepickerModule,
      LookupRoutingModule
    ],
    declarations: [
      CreatelookupComponent,
      SearchlookupComponent,
      EditlookupComponent
    ],
    providers: [LookupService]
  })

export class LookupModule { }
