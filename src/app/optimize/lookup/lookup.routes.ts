import { Route } from '@angular/router';
import { CreatelookupComponent } from './createlookup.component';
import { SearchlookupComponent } from './searchlookup.component';
import { EditlookupComponent } from './editlookup.component';

export const LookupRoutes: Route[] = [
    {
        path: 'searchlookup',
        component: SearchlookupComponent
    },
    {
        path: 'createlookup',
        component: CreatelookupComponent
    },
    {
        path: 'editlookup',
        component: EditlookupComponent
    }
];

