import { LookupModel } from './lookupmodel';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LookupService {

  constructor(private http: HttpClient) { }

  // private baseUrl = 'http://localhost:8080/lookup';
  private baseUrl = 'http://3.17.60.48:8080/lookup';

  createLookup(lookupmodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/createLookup`, lookupmodel);
  }

  searchLookup(lookupmodel: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/searchLookup`, lookupmodel);
  }

  updateLookup(lookupmodel: Object): Observable<any> {
    return this.http.put(`${this.baseUrl}` + `/updateLookup`, lookupmodel);
  }

  getAllLookups(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/getAllLookups`);
  }

  editLookup(id) {

    return this
          .http
          .get(`${this.baseUrl}/${id}`);
    }
}
