import { LookupTestModule } from './lookup-test.module';

describe('LookupTestModule', () => {
  let lookupTestModule: LookupTestModule;

  beforeEach(() => {
    lookupTestModule = new LookupTestModule();
  });

  it('should create an instance', () => {
    expect(lookupTestModule).toBeTruthy();
  });
});
