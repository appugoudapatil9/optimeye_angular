import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LookupTestRoutingModule } from './lookup-test-routing.module';

@NgModule({
  imports: [
    CommonModule,
    LookupTestRoutingModule
  ],
  declarations: []
})
export class LookupTestModule { }
