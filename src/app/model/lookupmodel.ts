
    export class LookupModel {
        lookupId: number;
        lookupType: string;
        lookupName: string;
        lookupDescription: string;
        activeFlag: CharacterData;
        startDate: Date;
        endDate: Date;
    }
